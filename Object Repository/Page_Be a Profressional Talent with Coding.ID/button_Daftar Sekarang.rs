<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Daftar Sekarang</name>
   <tag></tag>
   <elementGuidId>b3a9f82c-326f-4a7b-9674-121dddeb11e0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@onclick=&quot;window.location.href='https://demo-app.online/register_bootcamp/homebeasiswa';&quot;]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.daftarBootcamp.daftarBootcamp--see-all.daftarBootcamp--scholarship</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>caed75e1-c5b1-4e46-a031-aa0e040c234a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-btn</name>
      <type>Main</type>
      <value>daftarSekarangBeasiswa</value>
      <webElementGuid>097545aa-ce5a-4b9b-991f-599b0f1d4c10</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>window.location.href='https://demo-app.online/register_bootcamp/homebeasiswa';</value>
      <webElementGuid>e00b7cde-8609-414a-aac2-97f360e4e66f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>daftarBootcamp daftarBootcamp--see-all daftarBootcamp--scholarship</value>
      <webElementGuid>94768a56-6d40-46be-ba01-653a0af7a153</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    Daftar Sekarang
                    
                </value>
      <webElementGuid>ca56e8ec-718b-45b9-a890-7c7182eb4852</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;scholarship-block&quot;]/div[@class=&quot;scholarship-detail&quot;]/button[@class=&quot;daftarBootcamp daftarBootcamp--see-all daftarBootcamp--scholarship&quot;]</value>
      <webElementGuid>186b6c9a-28e7-4c8e-99bd-aebe93a982c2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@onclick=&quot;window.location.href='https://demo-app.online/register_bootcamp/homebeasiswa';&quot;]</value>
      <webElementGuid>7d49e8b4-2d7a-4162-9470-f1f80ef6b107</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div[3]/div/button</value>
      <webElementGuid>557d435d-e080-4d71-86f2-de5fcc6410f6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dapatkan beasiswa 100%'])[1]/following::button[1]</value>
      <webElementGuid>228dba50-d478-4f95-8b8f-7912a56b83db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Metode Pembayaran'])[3]/preceding::button[1]</value>
      <webElementGuid>5ecbcc10-57b1-4acc-b634-20c9408fa4ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Income Share Agreement'])[1]/preceding::button[1]</value>
      <webElementGuid>2c4bd375-0fe1-452e-a6c8-03ca8b2de85b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/button</value>
      <webElementGuid>85799173-e5e1-4bae-aeb3-f9fb9fca5ccd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = '
                    Daftar Sekarang
                    
                ' or . = '
                    Daftar Sekarang
                    
                ')]</value>
      <webElementGuid>745ba6c7-43f7-45f5-8181-e5b9a97a981a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
